# Runbook
If this trigger an error.

- Check that last [elasticsearch-curation](https://gitlab.com/redsmin-cron/elasticsearch-curation) went right
- Check that [production-monitor](https://console.clever-cloud.com/organisations/orga_0d05c831-2d8f-4ee8-b4fb-235bae203917/applications/app_3890670c-e4b8-482d-b3e5-cde15667f816/logs) works, restart it otherwise.